# Course 1

## Day 1: Live lecture

### Topic for discussion: 

* Internet
* Git
* HTML
* CSS
* Bootstrap
* Responsiveness

## Day 2-3: Assignment Day

### Git Assignment [Deadline Day 2]

Focused on:

* Cloning
* Forking 
* Branches
* Merge Request
* Reverting Merge
* Rebase 

### HTML Assignment [Deadline Day 3]

Focused on:

* HTML
* CSS 
* Hyperlinking
* Embedding Media
* Responsive design

## Day 4: Live lecture

### Topic for discussion: 

* Desinging HTML from Wireframe
* Overview of Javascript

## Day 5: Assignment Day

### HTML App Assignment [Deadline Day 5]

Focused on:

* Creating end-to-end HTML App
* Develop as per design 
* Using third-party libraries
* Overidding bootstrap theme

## Day 6: Live lecture

### Topic for discussion: 

* Javascript
* Jquery
* What are APIs
* Ajax
* API Integration

## Day 7: Assignment Day

### API Assignment [Deadline Day 7]

Focused on:

* Javascript
* Ajax
* Data binding

## Day 8: Live lecture

### Topic for discussion: 

* Deployment on Heroku/Netlify
* Auto deployment
* Overview about Angular 
* Overview about React

## Day 9: Assignment Day

### Deployment Assignment [Deadline Day 5]

Focused on:

* Deployment

## Day 10: Discussion and doubt session over chat

